import junit.framework.TestCase;
import org.junit.Test;
public class tddTest extends TestCase {
	    @Test
	        public void testNormal() {
			        assertEquals("不及格", Myutil.percentage2fivegrade(55));
				        assertEquals("及格", Myutil.percentage2fivegrade(65));
					        assertEquals("中等", Myutil.percentage2fivegrade(75));
						        assertEquals("良好", Myutil.percentage2fivegrade(85));
							        assertEquals("优秀", Myutil.percentage2fivegrade(95));
								    }
	        @Test
		    public void testWrong(){
			            assertEquals("错误",Myutil.percentage2fivegrade(-10));
				            assertEquals("错误",Myutil.percentage2fivegrade(120));

					        }
		    @Test
		        public void testboundary(){
				        assertEquals("不及格",Myutil.percentage2fivegrade(0));
					        assertEquals("及格",Myutil.percentage2fivegrade(60));
						        assertEquals("中等",Myutil.percentage2fivegrade(70));
							        assertEquals("良好",Myutil.percentage2fivegrade(80));
								        assertEquals("优秀",Myutil.percentage2fivegrade(90));
									        assertEquals("优秀",Myutil.percentage2fivegrade(100));
										    }
}
