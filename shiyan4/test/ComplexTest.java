import junit.framework.TestCase;
import org.junit.Test;
public class ComplexTest extends TestCase {
	    Complex a= new Complex(1.0,2.0);
	        @Test

		    public void testgetrealComplex(){
			            assertEquals(1.0,a.getrealComplex());
				        }
		    @Test
		        public void testgetimageComplex(){
				        assertEquals(2.0,a.getimageComplex());
					    }
		        @Test
			    public void testgetComplexadd(){
				            assertEquals("2.0+4.0i",a.getComplexadd(1.0,2.0));
					        }
			    @Test
			        public void testgetComplexsub(){
					        assertEquals("2.0+4.0i",a.getComplexsub(3.0,6.0));
						    }
			        @Test
				    public void testgetComplexmulti(){
					            assertEquals("-3.0+5.0i",a.getComplexmulti(1.0,2.0));
						        }
				    @Test
				        public void testgetComplexdiv(){
						        assertEquals("1.3333333333333333+1.6666666666666667i",a.getComplexdiv(2.0,1.0));
							    }
}
