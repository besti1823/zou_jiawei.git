package work9;

import java.util.concurrent.TimeUnit;

public class BSTNode<T extends Comparable<T>> extends BTNode<T> {
    public BSTNode(T element) {
        super(element);
    }

    public void add(T item) {
        if (item.compareTo(element) < 0)
            if (left == null)
                left = new BSTNode<T>(item);
            else
                ((BSTNode) left).add(item);
        else if (right == null)
            right = new BSTNode(item);
        else
            ((BSTNode) right).add(item);
    }

    public BSTNode<T> find(T target) {
        BSTNode<T> result = null;
        if (target.compareTo(element) == 0)
            result = this;
        else {
            if (target.compareTo(element) == 0)
                result = this;
            else {
                if (target.compareTo(element) < 0) {
                    if (left != null)
                        result = ((BSTNode) left).find(target);
                } else if (right != null)
                    result = ((BSTNode) right).find(target);
            }
        }
        return result;
    }
//    public BSTNode<T> remove(T target){
//        BSTNode<T> result=this;
//        if(target.compareTo(element)==0){
//            if(left==null)
//        }
    }
//}
