package work9;

import exp8._1.E;

public class LinkedBinarySearchingTree<T extends Comparable<T>>extends LinkedBinaryTree<T>  {

    public LinkedBinarySearchingTree(){
        super();
    }
    public LinkedBinarySearchingTree(T element){
        root=new BSTNode<T>(element);
    }
    public void add(T item){
        if(root==null)
            root=new BSTNode<T>(item);
        else
            ((BSTNode)root).add(item);
    }
//    public T remove(T target) throws E {
//        BSTNode<T> node=null;
//        if(root!=null)
//            node=((BSTNode)root).find(target);
//        if(node==null)
//            throw new E("Wrong");
//        root=(BSTNode)root.remove(target);
//    }
        T biggest;
    T small;
    int aaa=1;
    int aa;
    public T findmax(BSTNode temp){
        if(aaa==1) biggest= (T) temp.element;
        aaa++;
        if(temp==null) return null;
        else {

            findmax((BSTNode)temp.left);
            if(biggest.compareTo( (T) temp.element)<0){
                biggest=(T)temp.element;
            }
            findmax((BSTNode)temp.right);
        }
        return biggest;
        }
    public T findmin(BSTNode temp){
        if(aa==0) small= (T) temp.element;
        aa++;
        if(temp==null) return null;
        else {

            findmin((BSTNode)temp.left);
            if(small.compareTo( (T) temp.element)>0){
                small=(T)temp.element;
            }
            findmin((BSTNode)temp.right);
        }
        return small;
    }
}
