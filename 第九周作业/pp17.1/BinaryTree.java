package work9;

import exp8._1.E;

import java.util.ArrayList;


public interface BinaryTree<T> {
    public T getRootElement() throws E;
    public BinaryTree<T> getRihgt() throws E;
    public boolean contains(T target);
    public T find(T target) throws E;
    public boolean isEmpty();
    public int size();
    public String toString();
    public ArrayList<T> preorder();
    public ArrayList<T> inorder();
    public ArrayList<T> postorder();
}
