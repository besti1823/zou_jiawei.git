package work9;

import exp8._1.E;
import pp14.LinkedQueue;

import java.util.ArrayList;
import java.util.Iterator;

public class LinkedBinaryTree<T> implements BinaryTree<T> {
    public BTNode<T> root;
    //    protected BTNode<T> root;
    public LinkedBinaryTree(){
        root=null;
    }
    public LinkedBinaryTree(T element){
        root=new BTNode<T>( element);

    }
    public LinkedBinaryTree(T element,LinkedBinaryTree<T> left,LinkedBinaryTree<T> right){
        root=new BTNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }
    public void setleft(LinkedBinaryTree<T> temp){
        root.setLeft(temp.root);
    }
    public void setright(LinkedBinaryTree<T> temp){
        root.setRight(temp.root);
    }
    public T getRootElement()throws E {
        if(root==null)
            throw new E("the tree is empty");
        return root.getElement();
    }


    public LinkedBinaryTree<T> getLeft()throws E{
        if(root==null)throw new E("the tree is empty");
        LinkedBinaryTree<T> result=new LinkedBinaryTree<T>();
        result.root=root.getLeft();
        return result;
    }
    public T find(T target){
        BTNode<T> node=null;
        if(root!=null)
            node=root.find(target);
//        if(node==null)
//            throw new E("the tree is empty");
        return node.getElement();
    }

    public ArrayList<T> inorder(){
        ArrayList<T> iter=new ArrayList<T>();
        if(root!=null)
            root.inorder(iter);
        return iter;

    }

    public ArrayList<T> levelorder(){
        LinkedQueue<BTNode<T>> queue=new LinkedQueue<BTNode<T>>();
        ArrayList<T> iter=new ArrayList<>();
        if(root!=null){
            queue.enqueue(root);
            while(!queue.isEmpty()){
                BTNode<T> current=queue.dequeue();
                iter.add(current.getElement());
                if(current.getLeft()!=null)
                    queue.enqueue(current.getLeft());
                if(current.getRight()!=null)
                    queue.enqueue(current.getRight());
            }
        }
        return iter;
    }
    public int size(){
        int result=0;
        if(root!=null)
            result=root.count();
        return result;
    }

    public void okk(LinkedBinaryTree kk) throws E {
        if(kk.root==null){
            return;
        }
        else{okk(kk.getLeft());

            okk(kk.getRihgt());
            System.out.print(kk.getRootElement()+" ");}
    }
    @Override
    public LinkedBinaryTree<T> getRihgt() throws E{
        if(root==null)throw new E("the tree is empty");
        LinkedBinaryTree<T> result=new LinkedBinaryTree<T>();
        result.root=root.getRight();
        return result;
    }

    @Override
    public boolean contains(T target) {
        BTNode temp;
        temp=root.find(target);
        if(temp.element==target)
            return true;
        else return false;
    }

    @Override
    public boolean isEmpty() {
        if(root==null)
            return true;
        else
            return  false;
    }


    @Override
    public ArrayList<T> preorder() {
        ArrayList<T> iter=new ArrayList<>();
        if(root!=null){
            root.preorder(iter);
        }
        return iter;
    }
    @Override
    public ArrayList<T> postorder() {
        ArrayList<T> iter=new ArrayList<>();
        if(root!=null){
            root.postorder(iter);
        }
        return iter;
    }
    public LinkedBinaryTree<T> creat(String pre,String in){
        String a= String.valueOf(pre.charAt(0));
        int b=in.indexOf(a);
        String preleft=pre.substring(1,b+1);
        String preright=pre.substring(b+1);
        String inleft=in.substring(0,b);
        String inright=in.substring(b+1);
        LinkedBinaryTree<T> lefttree=new LinkedBinaryTree<T>();
        LinkedBinaryTree<T> rightree=new LinkedBinaryTree<T>();
        if(inleft.length()>1){
            lefttree=lefttree.creat(preleft,inleft);
        }
        else if(inleft.length()==1)
            lefttree=new LinkedBinaryTree<T>((T) inleft);
        if(inright.length()>1){
            rightree=rightree.creat(preright,inright);
        }
        else if(inright.length()==1)
            rightree=new LinkedBinaryTree<T>((T) inright);
        LinkedBinaryTree result=new LinkedBinaryTree<T>((T)a,lefttree,rightree);
        return  result;
    }

}