package shijian1130;

public class Edge{
    public String to;
    public Edge nextEdge;

    public Edge(String to) {
        this.to = to;
        this.nextEdge=null;
    }
}