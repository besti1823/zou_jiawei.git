package shijian1130;

import java.util.*;


class Graph {

    public Vertex[] v ;
    public  Edge[] e;
    public  int edgeNumber;
    public  int vertexNumber;



    //根据输入建立一个有向图
    public void buildGraph()
    {
        System.out.print("输入顶点数和边数：");
        Scanner s = new Scanner(System.in);
        vertexNumber = s.nextInt();
        edgeNumber = s.nextInt();
        System.out.println();
        //建立顶点数组
        v = new Vertex[vertexNumber];
        e = new Edge[edgeNumber];

        System.out.println("输入节点名称：");

        for(int i = 0;i < vertexNumber ;  i++)
        {
            String name = s.next();
            v[i] = new Vertex(name);
            // v[i].vIndex = i;
        }


        for(int i = 0;i< edgeNumber ; i++)
        {
            System.out.print("输入起点和终点:");
            String startVertex = s.next();
            String endVertex = s.next();

            //找到起点的vertex索引值
            int vBeginIndex=findvIndex(startVertex),
                    vEndIndex=findvIndex(endVertex);


            e[i]= new Edge(endVertex);//由终点建立到该终点的边
            v[vEndIndex].indegree++;//相应Vertex的入度+1
            //e[i].eIndex = i;

            e[i].nextEdge = v[vBeginIndex].first;//将该边的下一条边连接到以startVertex的所有边
            v[vBeginIndex].first = e[i];//将e作为v[startVertex]的第一条边
        }
    }

    public int getLength()
    {
        return v.length;
    }

    //返回一个包含相邻节点的ArrayList
    public ArrayList<Vertex> getAdjacentVertex(Vertex ver)
    {
        int index ;
        ArrayList al = new ArrayList();

        //找到指向ver的相邻节点
        for(int j=0; j < v.length;j++)
        {
            if(v[j].first != null )
                for(Edge e = v[j].first;e!=null;e= e.nextEdge)
                    if(e.to.equals(ver.from))
                    {
                        al.add(v[j]);
                    }
        }
        index = findvIndex(ver.from);
        //找到以ver为起点指向的相邻节点
        for(Edge e = v[index].first;e != null; e = e.nextEdge)
        {
            al.add( v[findvIndex(e.to)] );
        }

        return al;

    }

    //返回一个节点的入度（有几个节点直接指向该节点）
    public int getEnterEdgeNumber(Vertex ver)
    {
        int counter =0 ;
        for(int i =0;i<edgeNumber; i++)
        {
            if(e[i].to.equals(ver.from))
                counter++;
        }
        return counter;
    }

    public int findvIndex(String s)
    {
        int vIndex=-1;
        for(int j=0;j<v.length;j++)
        {
            if(v[j].from.equals(s))
                vIndex = j;
        }
        return vIndex;
    }

}