package shijian1130;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;


public class TopSort {

    Graph g ;

    public TopSort(Graph g)
    {
        this.g = g;
        g.buildGraph();
    }

    public void topSort()
    {
        Queue<Vertex> queue= new LinkedList<Vertex>();

        int length = g.getLength();
        for(int i=0;i<length;i++)
        {
            //首先把入度为0的所有节点入队
            if(g.getEnterEdgeNumber(g.v[i])==0)
            {
                queue.add(g.v[i]);
            }
        }

        System.out.print("拓扑顺序为：");
        while(!queue.isEmpty())
        {
            //队首出列，并找到相邻节点
            Vertex ver = queue.poll();
            ArrayList<Vertex> al = g.getAdjacentVertex(ver);
            for (Vertex vertex : al) {
                System.out.print(ver.from + " ");
                if (--vertex.indegree == 0)
                    queue.add(vertex);
            }

        }
        if (!queue.isEmpty()){
            System.out.println("存在环！");
        }
    }
}