public class pp9_7_2 implements pp9_7_1,Comparable {
    int a;
    int b;
    String c;
    public pp9_7_2(String c){
        this.c=c;
    }
    public String toString(){
        return c;
    }
    @Override
    public void getPriority(int a) {
        this.a=a;
    }

    @Override
    public int setPriority() {
        return a;
    }

    @Override
    public int compareTo(Object o) {
        this.b=(int)o;
        return this.a-b;
    }
}
