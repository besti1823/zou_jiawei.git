public class Employee extends StaffMember {
    protected String socialSecuritynember;
    protected double payRate;
    public Employee(String eName ,String eAddress,String ePhone,String socSecNumber,double rate){
        super(eName,eAddress,ePhone);
        socialSecuritynember=socSecNumber;
        payRate=rate;
    }
    public String toString(){
        String result=super.toString();
        result+="\nSocial Security Number:"+socialSecuritynember;
        return  result;
    }
    public double pay(){
        return  payRate;
    }

    @Override
    public int vacation() {
        return 2;
    }
}
