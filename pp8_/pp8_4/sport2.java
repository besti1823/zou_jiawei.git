public class sport2 extends sport1 {
    private String a;
    private String b;
    public sport2(String a, String b) {
        this.a = a;
        this.b = b;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    @Override
    public void run() {
        System.out.println(a+"need"+"run");
    }
    @Override
    public void swim(){
        System.out.println(b+"need swim");
    }
}
