public class thing2 extends things {
    private int a;
    private int b;
    private int c;
    private int d;
    public thing2(int a, int b, int c,int d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d =d;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    @Override
    public void ball() {
        double e,f;
        e=4/3*3.14*d*d*d;
        f=4*3.14*e*e;
        System.out.println("球的体积为："+e+"球的面积为："+f);
    }

    @Override
    public void changfangti() {
    int e,f,g;
    e=a*b*c;
    f=2*(a*b+b*c+c*a);
    g=2*(a+b+c);
    System.out.println("长方体体积为："+e+" 面积为："+f+" 周长为："+g);
    }
}
