package shijian2;
public class leaf {
    char element;
    leaf left;
    leaf right;
    public leaf(char element, leaf left, leaf right) {
        this.element = element;
        this.left = left;
        this.right = right;
    }

    public leaf(char element) {
        this.element = element;
        this.left = null;
        this.right = null;
    }
}