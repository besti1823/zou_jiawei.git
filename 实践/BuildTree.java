package shijian2;

import java.util.LinkedList;

public class BuildTree {
    private static char list[];
    private static int size;
    private leaf root;
    static int i = 0;
    static LinkedList<leaf> lookList;

    public BuildTree(char[] list) {
        this.list = list;
        root=new leaf(list[0]);
        size= list.length;
        lookList=new LinkedList<>();
    }

    public static leaf creat(leaf node,char element[]) {
        char temp=element[i++];
        if (temp=='#'){
            return null;
        }
        else {
            node=new leaf(temp);
            node.left=creat(node.left,element);
            node.right=creat(node.right,element);
        }
        return node;
    }

    public static void look(leaf node) {
        if (node == null) {
            return;
        }
        lookList.add(node);
        while(!lookList.isEmpty()){
            leaf temp = lookList.poll();
            System.out.print(temp.element +" ");
            if(temp.left!=null)lookList.add(temp.left);
            if(temp.right!=null)lookList.add(temp.right);
        }
    }
}