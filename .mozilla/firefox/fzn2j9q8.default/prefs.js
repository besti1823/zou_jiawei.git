// Mozilla User Preferences

// DO NOT EDIT THIS FILE.
//
// If you make changes to this file while the application is running,
// the changes will be overwritten when the application exits.
//
// To change a preference value, you can either:
// - modify it via the UI (e.g. via about:config in the browser); or
// - set it within a user.js file in your profile.

user_pref("app.normandy.first_run", false);
user_pref("app.normandy.startup_delay_seconds", 300);
user_pref("app.normandy.user_id", "854a5505-feb4-44aa-b081-9b0135e8d32d");
user_pref("app.update.lastUpdateTime.addon-background-update-timer", 1568099028);
user_pref("app.update.lastUpdateTime.blocklist-background-update-timer", 1568099148);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 1568104584);
user_pref("app.update.lastUpdateTime.experiments-update-timer", 1568098908);
user_pref("app.update.lastUpdateTime.recipe-client-addon-run", 1568104344);
user_pref("app.update.lastUpdateTime.search-engine-update-timer", 1568098668);
user_pref("app.update.lastUpdateTime.services-settings-poll-changes", 1568104464);
user_pref("app.update.lastUpdateTime.telemetry_modules_ping", 1568015820);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 1568099268);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.cache.disk.amount_written", 1073);
user_pref("browser.cache.disk.capacity", 757760);
user_pref("browser.cache.disk.filesystem_reported", 1);
user_pref("browser.cache.disk.smart_size.first_run", false);
user_pref("browser.cache.frecency_experiment", 2);
user_pref("browser.contentblocking.category", "standard");
user_pref("browser.ctrlTab.migrated", true);
user_pref("browser.ctrlTab.recentlyUsedOrder", false);
user_pref("browser.download.importedFromSqlite", true);
user_pref("browser.laterrun.bookkeeping.profileCreationTime", 1567863658);
user_pref("browser.laterrun.bookkeeping.sessionCount", 9);
user_pref("browser.laterrun.enabled", true);
user_pref("browser.migration.version", 86);
user_pref("browser.newtabpage.activity-stream.impressionId", "{803b3eff-6ccf-45b5-9b6f-993f01ba6a44}");
user_pref("browser.newtabpage.enhanced", true);
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pageActions.persistedActions", "{\"version\":1,\"ids\":[\"bookmark\",\"pinTab\",\"bookmarkSeparator\",\"copyURL\",\"emailLink\",\"addSearchEngine\",\"sendToDevice\",\"pocket\",\"screenshots_mozilla_org\"],\"idsInUrlbar\":[\"pocket\",\"bookmark\"]}");
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.places.smartBookmarksVersion", 8);
user_pref("browser.rights.3.shown", true);
user_pref("browser.safebrowsing.provider.google4.lastupdatetime", "1568104373481");
user_pref("browser.safebrowsing.provider.google4.nextupdatetime", "1568106181481");
user_pref("browser.safebrowsing.provider.mozilla.lastupdatetime", "1568104327588");
user_pref("browser.safebrowsing.provider.mozilla.nextupdatetime", "1568107927588");
user_pref("browser.search.region", "CN");
user_pref("browser.search.widget.inNavBar", true);
user_pref("browser.selfsupport.enabled", false);
user_pref("browser.sessionstore.upgradeBackup.latestBuildID", "20190828111117");
user_pref("browser.shell.mostRecentDateSetAsDefault", "1568105263");
user_pref("browser.slowStartup.averageTime", 4567);
user_pref("browser.slowStartup.samples", 4);
user_pref("browser.startup.homepage_override.buildID", "20190828111117");
user_pref("browser.startup.homepage_override.mstone", "69.0");
user_pref("browser.startup.lastColdStartupCheck", 1568105260);
user_pref("browser.tabs.remote.autostart.2", true);
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"home-button\",\"customizableui-special-spring1\",\"urlbar-container\",\"search-container\",\"customizableui-special-spring2\",\"downloads-button\",\"library-button\",\"sidebar-button\",\"fxa-toolbar-menu-button\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[\"personal-bookmarks\"]},\"seen\":[\"pocket-button\",\"developer-button\"],\"dirtyAreaCache\":[\"PersonalToolbar\",\"nav-bar\",\"TabsToolbar\",\"toolbar-menubar\",\"PanelUI-contents\",\"addon-bar\"],\"currentVersion\":16,\"newElementCount\":2}");
user_pref("browser.urlbar.daysBeforeHidingSuggestionsPrompt", 2);
user_pref("browser.urlbar.lastSuggestionsPromptDate", 20190909);
user_pref("browser.urlbar.matchBuckets", "general:5,suggestion:Infinity");
user_pref("browser.urlbar.placeholderName", "Google");
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1568012204949");
user_pref("datareporting.sessions.current.activeTicks", 243);
user_pref("datareporting.sessions.current.clean", true);
user_pref("datareporting.sessions.current.firstPaint", 3051);
user_pref("datareporting.sessions.current.main", 90);
user_pref("datareporting.sessions.current.sessionRestored", 3242);
user_pref("datareporting.sessions.current.startTime", "1568013155542");
user_pref("datareporting.sessions.current.totalTime", 88998);
user_pref("datareporting.sessions.currentIndex", 6);
user_pref("datareporting.sessions.previous.0", "{\"s\":1567863654173,\"a\":3,\"t\":21,\"c\":true,\"m\":878,\"fp\":7352,\"sr\":7116}");
user_pref("datareporting.sessions.previous.1", "{\"s\":1568012184001,\"a\":8,\"t\":94,\"c\":true,\"m\":1604,\"fp\":11629,\"sr\":10845}");
user_pref("datareporting.sessions.previous.2", "{\"s\":1568012290985,\"a\":18,\"t\":355,\"c\":true,\"m\":70,\"fp\":3150,\"sr\":3215}");
user_pref("datareporting.sessions.previous.3", "{\"s\":1568012647298,\"a\":5,\"t\":30,\"c\":true,\"m\":80,\"fp\":3955,\"sr\":3415}");
user_pref("datareporting.sessions.previous.4", "{\"s\":1568012678674,\"a\":11,\"t\":170,\"c\":true,\"m\":100,\"fp\":3752,\"sr\":3211}");
user_pref("datareporting.sessions.previous.5", "{\"s\":1568013021091,\"a\":7,\"t\":133,\"c\":true,\"m\":330,\"fp\":3156,\"sr\":3206}");
user_pref("devtools.onboarding.telemetry.logged", true);
user_pref("devtools.performance.recording.interval", 1000);
user_pref("distribution.canonical.bookmarksProcessed", true);
user_pref("distribution.iniFile.exists.appversion", "69.0");
user_pref("distribution.iniFile.exists.value", true);
user_pref("dom.push.userAgentID", "35a4029b0ca048e2a0aaf5e5e610a095");
user_pref("e10s.rollout.cohort", "disqualified-test");
user_pref("e10s.rollout.cohortSample", "0.760463");
user_pref("e10s.rollout.cohortSample.multi", "0.860026");
user_pref("experiments.activeExperiment", false);
user_pref("extensions.activeThemeID", "default-theme@mozilla.org");
user_pref("extensions.blocklist.pingCountTotal", 3);
user_pref("extensions.blocklist.pingCountVersion", -1);
user_pref("extensions.bootstrappedAddons", "{\"firefox@getpocket.com\":{\"version\":\"1.0.5\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox/browser/features/firefox@getpocket.com.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"screenshots@mozilla.org\":{\"version\":\"6.6.0\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox/browser/features/screenshots@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"webcompat@mozilla.org\":{\"version\":\"1.1\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox/browser/features/webcompat@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"aushelper@mozilla.org\":{\"version\":\"2.0\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox/browser/features/aushelper@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"shield-recipe-client@mozilla.org\":{\"version\":\"1.0.0\",\"type\":\"extension\",\"descriptor\":\"/home/sister/.mozilla/firefox/fzn2j9q8.default/features/{f02872de-533a-47a1-96e3-b47cdae95f0d}/shield-recipe-client@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"disable-crash-autosubmit@mozilla.org\":{\"version\":\"2.1\",\"type\":\"extension\",\"descriptor\":\"/home/sister/.mozilla/firefox/fzn2j9q8.default/features/{f02872de-533a-47a1-96e3-b47cdae95f0d}/disable-crash-autosubmit@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"e10srollout@mozilla.org\":{\"version\":\"1.85\",\"type\":\"extension\",\"descriptor\":\"/home/sister/.mozilla/firefox/fzn2j9q8.default/features/{f02872de-533a-47a1-96e3-b47cdae95f0d}/e10srollout@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"followonsearch@mozilla.com\":{\"version\":\"0.9.1\",\"type\":\"extension\",\"descriptor\":\"/home/sister/.mozilla/firefox/fzn2j9q8.default/features/{f02872de-533a-47a1-96e3-b47cdae95f0d}/followonsearch@mozilla.com.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"hotfix-bug-1548973@mozilla.org\":{\"version\":\"1.1.4\",\"type\":\"extension\",\"descriptor\":\"/home/sister/.mozilla/firefox/fzn2j9q8.default/features/{f02872de-533a-47a1-96e3-b47cdae95f0d}/hotfix-bug-1548973@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"timecop@mozilla.com\":{\"version\":\"1.0\",\"type\":\"extension\",\"descriptor\":\"/home/sister/.mozilla/firefox/fzn2j9q8.default/features/{f02872de-533a-47a1-96e3-b47cdae95f0d}/timecop@mozilla.com.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false}}");
user_pref("extensions.databaseSchema", 31);
user_pref("extensions.e10s.rollout.blocklist", "");
user_pref("extensions.e10s.rollout.hasAddon", false);
user_pref("extensions.e10s.rollout.policy", "50allmpc");
user_pref("extensions.e10sBlockedByAddons", true);
user_pref("extensions.e10sMultiBlockedByAddons", true);
user_pref("extensions.enabledAddons", "ubufox%40ubuntu.com:3.2,%7B972ce4c6-7e08-4474-a285-3208198ce6fd%7D:54.0");
user_pref("extensions.getAddons.databaseSchema", 5);
user_pref("extensions.incognito.migrated", true);
user_pref("extensions.lastAppBuildId", "20190828111117");
user_pref("extensions.lastAppVersion", "69.0");
user_pref("extensions.lastPlatformVersion", "69.0");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.webcompat.perform_injections", true);
user_pref("extensions.webcompat.perform_ua_overrides", true);
user_pref("extensions.webextensions.uuids", "{\"screenshots@mozilla.org\":\"7bae9810-b94f-47ff-9100-d0df095442da\",\"webcompat@mozilla.org\":\"83647447-a476-46ee-9bcf-841278222edb\",\"formautofill@mozilla.org\":\"6e4e81a6-34cc-434b-a91c-3d7bc464ea37\",\"fxmonitor@mozilla.org\":\"8955fd49-7750-41c0-9738-74b4fe121368\",\"webcompat-reporter@mozilla.org\":\"0ea31742-c1ab-43e3-8a41-efb40e7ba1ec\",\"default-theme@mozilla.org\":\"56d37940-26db-48a2-8f04-1424496c2de8\",\"google@search.mozilla.org\":\"4c9789bb-3062-4082-b2b2-20137d0ca4a8\",\"amazondotcom@search.mozilla.org\":\"9e16b252-37e4-4201-bb07-68720d7e7702\",\"bing@search.mozilla.org\":\"6804b90e-1702-4839-8002-d46f8f37c378\",\"ddg@search.mozilla.org\":\"d9debb94-047c-4c64-87e9-fd986f52f703\",\"ebay@search.mozilla.org\":\"13490e6e-8693-438b-836a-a62ab222a6df\",\"twitter@search.mozilla.org\":\"2c33130f-0779-4420-8dd3-4a811853661d\",\"wikipedia@search.mozilla.org\":\"00feed7d-459d-4a26-851e-d47a6c8fe8ee\"}");
user_pref("extensions.xpiState", "{\"app-system-defaults\":{\"firefox@getpocket.com\":{\"d\":\"/usr/lib/firefox/browser/features/firefox@getpocket.com.xpi\",\"e\":true,\"v\":\"1.0.5\",\"st\":1497273884000},\"screenshots@mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/features/screenshots@mozilla.org.xpi\",\"e\":true,\"v\":\"6.6.0\",\"st\":1497273884000},\"webcompat@mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/features/webcompat@mozilla.org.xpi\",\"e\":true,\"v\":\"1.1\",\"st\":1497273884000},\"aushelper@mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/features/aushelper@mozilla.org.xpi\",\"e\":true,\"v\":\"2.0\",\"st\":1497273883000},\"e10srollout@mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/features/e10srollout@mozilla.org.xpi\",\"e\":true,\"v\":\"1.50\",\"st\":1497273883000}},\"app-global\":{\"langpack-en-CA@firefox.mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/extensions/langpack-en-CA@firefox.mozilla.org.xpi\",\"e\":false,\"v\":\"69.0\",\"st\":1566995279000},\"langpack-en-GB@firefox.mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/extensions/langpack-en-GB@firefox.mozilla.org.xpi\",\"e\":false,\"v\":\"69.0\",\"st\":1566995279000},\"{972ce4c6-7e08-4474-a285-3208198ce6fd}\":{\"d\":\"/usr/lib/firefox/browser/extensions/{972ce4c6-7e08-4474-a285-3208198ce6fd}.xpi\",\"e\":true,\"v\":\"54.0\",\"st\":1497273883000}},\"app-system-share\":{\"ubufox@ubuntu.com\":{\"d\":\"/usr/share/mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/ubufox@ubuntu.com.xpi\",\"e\":true,\"v\":\"3.2\",\"st\":1442597402000}},\"app-system-addons\":{\"shield-recipe-client@mozilla.org\":{\"d\":\"/home/sister/.mozilla/firefox/fzn2j9q8.default/features/{f02872de-533a-47a1-96e3-b47cdae95f0d}/shield-recipe-client@mozilla.org.xpi\",\"e\":true,\"v\":\"1.0.0\",\"st\":1568012573000},\"disable-crash-autosubmit@mozilla.org\":{\"d\":\"/home/sister/.mozilla/firefox/fzn2j9q8.default/features/{f02872de-533a-47a1-96e3-b47cdae95f0d}/disable-crash-autosubmit@mozilla.org.xpi\",\"e\":true,\"v\":\"2.1\",\"st\":1568012573000},\"e10srollout@mozilla.org\":{\"d\":\"/home/sister/.mozilla/firefox/fzn2j9q8.default/features/{f02872de-533a-47a1-96e3-b47cdae95f0d}/e10srollout@mozilla.org.xpi\",\"e\":true,\"v\":\"1.85\",\"st\":1568012573000},\"followonsearch@mozilla.com\":{\"d\":\"/home/sister/.mozilla/firefox/fzn2j9q8.default/features/{f02872de-533a-47a1-96e3-b47cdae95f0d}/followonsearch@mozilla.com.xpi\",\"e\":true,\"v\":\"0.9.1\",\"st\":1568012573000},\"hotfix-bug-1548973@mozilla.org\":{\"d\":\"/home/sister/.mozilla/firefox/fzn2j9q8.default/features/{f02872de-533a-47a1-96e3-b47cdae95f0d}/hotfix-bug-1548973@mozilla.org.xpi\",\"e\":true,\"v\":\"1.1.4\",\"st\":1568012573000},\"timecop@mozilla.com\":{\"d\":\"/home/sister/.mozilla/firefox/fzn2j9q8.default/features/{f02872de-533a-47a1-96e3-b47cdae95f0d}/timecop@mozilla.com.xpi\",\"e\":true,\"v\":\"1.0\",\"st\":1568012573000}}}");
user_pref("font.internaluseonly.changed", false);
user_pref("gfx.blacklist.layers.opengl", 4);
user_pref("gfx.blacklist.layers.opengl.failureid", "FEATURE_FAILURE_SOFTWARE_GL");
user_pref("idle.lastDailyNotification", 1568104838);
user_pref("media.gmp-gmpopenh264.abi", "x86_64-gcc3");
user_pref("media.gmp-gmpopenh264.lastUpdate", 1568104502);
user_pref("media.gmp-gmpopenh264.version", "1.8.1");
user_pref("media.gmp-manager.buildID", "20190828111117");
user_pref("media.gmp-manager.lastCheck", 1568104500);
user_pref("media.gmp.storage.version.observed", 1);
user_pref("network.cookie.prefsMigrated", true);
user_pref("network.predictor.cleaned-up", true);
user_pref("pdfjs.enabledCache.state", false);
user_pref("pdfjs.migrationVersion", 2);
user_pref("pdfjs.previousHandler.alwaysAskBeforeHandling", true);
user_pref("pdfjs.previousHandler.preferredAction", 4);
user_pref("places.database.lastMaintenance", 1568014524);
user_pref("places.history.expiration.transient_current_max_pages", 112348);
user_pref("plugin.disable_full_page_plugin_for_types", "application/pdf");
user_pref("privacy.sanitize.pending", "[{\"id\":\"newtab-container\",\"itemsToClear\":[],\"options\":{}}]");
user_pref("security.sandbox.content.tempDirSuffix", "0aed8561-25c4-4771-9e24-aad0758c1d5e");
user_pref("security.sandbox.plugin.tempDirSuffix", "bbe1f15f-9b1c-48f1-a409-05284c3002a1");
user_pref("services.blocklist.clock_skew_seconds", 20);
user_pref("services.blocklist.gfx.checked", 1568099131);
user_pref("services.blocklist.last_update_seconds", 1568099131);
user_pref("services.blocklist.onecrl.checked", 1568099131);
user_pref("services.blocklist.pinning.checked", 1568099131);
user_pref("services.settings.clock_skew_seconds", -77);
user_pref("services.settings.last_etag", "\"1568051630234\"");
user_pref("services.settings.last_update_seconds", 1568104543);
user_pref("services.settings.main.anti-tracking-url-decoration.last_check", 1568104543);
user_pref("services.settings.main.cfr-fxa.last_check", 1568104543);
user_pref("services.settings.main.cfr.last_check", 1568104543);
user_pref("services.settings.main.fxmonitor-breaches.last_check", 1568104543);
user_pref("services.settings.main.hijack-blocklists.last_check", 1568104543);
user_pref("services.settings.main.language-dictionaries.last_check", 1568104543);
user_pref("services.settings.main.normandy-recipes.last_check", 1568104543);
user_pref("services.settings.main.sites-classification.last_check", 1568104543);
user_pref("services.settings.main.url-classifier-skip-urls.last_check", 1568104543);
user_pref("signon.importedFromSqlite", true);
user_pref("storage.vacuum.last.index", 1);
user_pref("storage.vacuum.last.places.sqlite", 1568014524);
user_pref("toolkit.startup.last_success", 1568105257);
user_pref("toolkit.telemetry.cachedClientID", "e3e40dde-969e-4fe9-b25a-556d272deee7");
user_pref("toolkit.telemetry.previousBuildID", "20190828111117");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("trailhead.extendedTriplets.experiment", "control");
