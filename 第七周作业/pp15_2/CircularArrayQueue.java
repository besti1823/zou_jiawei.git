package dui;

public class CircularArrayQueue<T> implements Queue<T> {
    private final int l=5;
    private int front ,rear,count;
    private T[] queue;
    public CircularArrayQueue(){
        front =rear=count=0;
        queue=(T[])(new Object[l]);
    }
    @Override
    public void enqueue(T ele)throws E {
        if(count==queue.length) throw  new E("has full");
        else{queue[rear]=ele;
        rear=(rear+1)%queue.length;
        count++;}
    }
    public String expandCapacity(){
        String a;
        a="full";
        return a;
    }
    @Override
    public T dequeue()throws E{
        if(count==0) throw new E("has empty");
        T a=queue[front];
        front=(front+1)%queue.length;
        count--;
        return a;
    }

    @Override
    public T first() {
        T a=queue[front];
        return a;
    }

    @Override
    public boolean isEmpty() {
        if(count==0)
        return true;
        else
            return false;
    }

    @Override
    public int size() {
        return count;
    }
}
