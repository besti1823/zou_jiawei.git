package pp14;


public class LinkfedStack<T> implements Stack<T> {
    private int count;
    private LinearNode<T> top;
    public LinkfedStack(){
        count=0;
        top=null;
    }
    @Override
    public T pop()throws E {
        if(count==0){throw new E("Wrong");}
        T result =top.getElement();
        top=top.getNext();
        count--;
        return result;
    }
    public  String toString(){
        String result="<top of stack>"+"\n";
        LinearNode current=top;
        while (current!=null){
            result += current.getElement()+" ";
            current=current.getNext();
        }
        return result+"\n"+"<bottom of stack>";
    }
    @Override
    public void push(T element) {
        LinearNode<T> temp=top;
        LinearNode<T> num1=new LinearNode(element);
        top=num1;
        top.setNext(temp);
        count++;
    }


    @Override
    public T peek() throws E {
        LinearNode<T> temp=top;
        if(temp.getElement()==null) throw new E("Wrong");
        T result=temp.getElement();
        return result;
    }

    @Override
    public boolean isEmpty() {
        if(count==0)
        return false;
        else
            return true;

    }

    @Override
    public int size() {
        return count;
    }
}
