public interface Stack<T> {
    public void push(T element);
    public T pop() throws E;
    public T peek() throws E;
    public boolean isEmpty();
    public int size();
    public String toString();
}
