import java.util.EmptyStackException;

public class ArrayStack<T> implements Stack<T> {
    private final int a=10;
    private  int count;
    private T[] stack;

    public ArrayStack(){
        count =0;
        stack=(T[])(new Object[a]);
    }
    @Override
    public void push(T element) {
        if(count==stack.length)
        expandCapacity();
        stack[count]=element;
        count++;
    }

    @Override
    public T pop()throws E {
        T b;
        String a="wrong";
        E problem = new E(a);
        if(count==0) {
            throw problem;
        }
        b=stack[count];
        stack[count]=null;
        count--;
        return b;
    }
    @Override
    public T peek()throws E{
        E problem=new E("wrong");
        if(count==0) {throw problem;}
        return stack[count-1];
    }

    @Override
    public boolean isEmpty() {

        if (count==0)return true;
        else return false;
    }


    @Override
    public int size() {

        return count;
    }
    private void expandCapacity(){
        T[] larger=(T[])(new Object[stack.length*2]);
        for(int index=0;index<stack.length;index++){
            larger[index]=stack[index];
            stack=larger;
        }
    }
    public String toString(){
        String result="<top of stack>\n";
        for(int index=count-1;index>=0;index--){
            result += stack[index]+"\n";
        }
        return result+"<bottom of stack>";
    }
}
