package pp14;

public class LinkedQueue<T> implements Queue<T> {
    private int count;
    private LinearNode<T> front,rear;
    public LinkedQueue(){
        count=0;
        front=rear=null;
    }
    public void enqueue(T element){
        LinearNode<T> node=new LinearNode<T>(element);
        if(count==0)
            front=node;
        else
            rear.setNext(node);
        rear=node;
        count++;
    }

    @Override
    public T dequeue() {
        T kk=front.getElement();
        front=front.getNext();
        count--;
        return kk;
    }

    @Override
    public T first() {
        T kk=front.getElement();
        return kk;
    }

    @Override
    public boolean isEmpty() {
        if(count==0)
        return true;
        else return false;
    }

    @Override
    public int size() {
        return count;
    }
    public void oo(){
        T kk=null;
        LinearNode<T> n=front;
        n.setNext(front.getNext());
        for(int aa=0;aa<=count-1;aa++){
            kk=n.getElement();
            System.out.println(kk+"\n");
            n=n.getNext();
        }
    }
    public  String toString(){
        String kk=" ";
        T kko;
        LinearNode<T> n=front;
        for(int aa=0;aa<=count-1;aa++){
            kko=n.getElement();
            kk=kk+kko;
            n=n.getNext();
        }
        return kk+"\n"+"<bottom of stack>";
    }
}