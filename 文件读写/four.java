import java.io.*;

/**
 * Created by besti on 2018/5/6.
 */
public class four {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("D:\\filetest","HelloWorld.txt");
        //File file = new File("HelloWorld.txt");
//        File file1 = new File("C:\\Users\\besti\\Desktop\\FileTest\\Test\\Test");
//        file1.mkdir();
//        file1.mkdirs();
        String content = "";
        if (!file.exists()){
            file.createNewFile();
        }
        Reader reader2 = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader2);
        System.out.println("\n下面是用BufferedReader读出的数据：");
        while ((content =bufferedReader.readLine())!= null){

            System.out.println(content);
            System.out.println("1");
        }
    }
}