import java.io.*;

/**
 * Created by besti on 2018/5/6.
 */
public class three{
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("D:\\filetest", "HelloWorld.txt");
        //File file = new File("HelloWorld.txt");
//        File file1 = new File("C:\\Users\\besti\\Desktop\\FileTest\\Test\\Test");
//        file1.mkdir();
//        file1.mkdirs();

        if (!file.exists()) {
            file.createNewFile();
        }
        Writer writer2 = new FileWriter(file);
        writer2.write("Hello, I/O Operataion!这是利用Writer写入文件的内容");
        writer2.flush();
        writer2.append("Hello,World");//append在后面添加字符串。也为写入
        writer2.flush();//字符流写入          第三种写入方法

        BufferedWriter bufferedWriter = new BufferedWriter(writer2);
        String content3 = "使用bufferedWriter写入";
        bufferedWriter.write(content3, 0, content3.length());
        bufferedWriter.flush();
        bufferedWriter.close();//在第三种情况下补充字符串

        Reader reader2 = new FileReader(file);
        System.out.println("下面是用Reader读出的数据：");
        while (reader2.ready()) {
            System.out.print((char) reader2.read() + "  ");//第三种读出方法
        }
    }}