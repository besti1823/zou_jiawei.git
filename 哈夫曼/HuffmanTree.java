package hafuman;

import java.util.*;

public class HuffmanTree {
    public static HuffNode createTree(List<HuffNode> nodes) {
       //先获取两个最小的结点，再存入新的节点。
        while (nodes.size() > 1) {
            //进行从大到小的排序
            Collections.sort(nodes);
            //获取
            HuffNode left = nodes.get(nodes.size() - 1);
            HuffNode right = nodes.get(nodes.size() - 2);
            //求和
            HuffNode parent = new HuffNode('无', left.getWeight() + right.getWeight());
            //让新节点作为两个权值最小节点的父节点
            parent.setLeft(left);
            left.setCodenumber("0");
            parent.setRight(right);
            right.setCodenumber("1");
            //删除
            nodes.remove(left);
            nodes.remove(right);
            //重新写入
            nodes.add(parent);
        }
        return nodes.get(0);
    }


    public static  List<HuffNode> breadthFirstTraversal(HuffNode root) {
        List<HuffNode> list = new ArrayList<HuffNode>();
        Queue<HuffNode> queue = new ArrayDeque<HuffNode>();

        //将根元素加入“队列
        if (root != null) {
            queue.offer(root);
            root.getLeft().setCodenumber(root.getCodenumber() + "0");
            root.getRight().setCodenumber(root.getCodenumber() + "1");
        }

        while (!queue.isEmpty()) {
            //将该队列的“队尾”元素加入到list中
            list.add(queue.peek());
            HuffNode node = queue.poll();

            //如果左子节点不为null，将它加入到队列
            if (node.getLeft() != null) {
                queue.offer(node.getLeft());
                node.getLeft().setCodenumber(node.getCodenumber() + "0");
            }
            //如果右子节点不为null，将它加入到队列
            if (node.getRight() != null) {
                queue.offer(node.getRight());
                node.getRight().setCodenumber(node.getCodenumber() + "1");
            }
        }
        return list;
    }
}