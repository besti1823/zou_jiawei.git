package hafuman;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static hafuman.HuffmanTree.breadthFirstTraversal;

public class HuffmanTest {
    public static void main(String[] args) throws IOException {
        //把字符集从文件中读出来，并保存在一个数组characters里面
        File file = new File("D:\\Huffman.txt");
        if(!file.exists()){
            file.createNewFile();
        }

        Reader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String temp = bufferedReader.readLine();

        char characters[] = new char[temp.length()];
        for (int i = 0; i < temp.length(); i++) {
            characters[i] = temp.charAt(i);
        }
        System.out.println("原字符集为：" + Arrays.toString(characters));

        //计算每一个字符出现的频率。并把出现的概率存在另一个数组中
        double frequency[] = new double[27];
        int numbers = 0;
        for (int i = 0; i < characters.length; i++) {
            if (characters[i] == ' ') {
                numbers++;
            }
            frequency[26] = (float) numbers / characters.length;
        }

        for (int j = 97; j <= 122; j++) {
            int number = 0;//给字母计数
            for (int m = 0; m < characters.length; m++) {
                if (characters[m] == (char) j) {
                    number++;
                }
                frequency[j - 97] = (float) number / characters.length;
            }
        }

        System.out.println("\n每一个字符对应的概率为（26字母+空格）" + "\n" + Arrays.toString(frequency));
        List<HuffNode> nodes = new ArrayList<>();
        for (int o = 97; o <= 122; o++) {
            nodes.add(new HuffNode((char) o, frequency[o - 97]));
        }
        nodes.add(new HuffNode(' ', frequency[26]));

        HuffNode root = HuffmanTree.createTree(nodes);
        String result1 = "";
        List<HuffNode> temp1 = breadthFirstTraversal(root);

        for (int i = 0; i < characters.length; i++) {
            for (int j = 0; j < temp1.size(); j++) {

                if (characters[i] == temp1.get(j).getData()) {
                    result1 += temp1.get(j).getCodenumber();
                }
            }
        }
        System.out.println("\n对文件进行编码后的结果为：");
        System.out.println(result1);
        File file2 = new File("D:\\enHuffman.txt");
        Writer writer = new FileWriter(file2);
        writer.write(result1);
        writer.close();

//将所有具有字符的叶子节点重新保存在一个newlist里面
        List<String> newlist = new ArrayList<>();
        for(int m=0;m < temp1.size();m++)
        {
            if(temp1.get(m).getData()!='无')
                newlist.add(String.valueOf(temp1.get(m).getData()));
        }
        System.out.println("\n字符："+newlist);

        List<String> newlist1 = new ArrayList<>();
        for(int m=0;m < temp1.size();m++)
        {
            if(temp1.get(m).getData()!='无')
                newlist1.add(String.valueOf(temp1.get(m).getCodenumber()));
        }
        System.out.println("\n对应编码："+newlist1);
        FileReader fileReader = new FileReader("D:\\enHuffman.txt");
        BufferedReader bufferedReader1 = new BufferedReader(fileReader);
        String secretline = bufferedReader1.readLine();

        List<String> secretText = new ArrayList<String>();
        for (int i = 0; i < secretline.length(); i++) {
            secretText.add(secretline.charAt(i) + "");
        }

        //解密
        String result2 = "";
        String current="";// linshizhi
        while(secretText.size()>0) {
            current = current + "" + secretText.get(0);
            secretText.remove(0);
            for (int p = 0; p < newlist1.size(); p++) {
                if (current.equals(newlist1.get(p))) {
                    result2 = result2 + "" + newlist.get(p);
                    current="";
                }

            }
        }
//最后输出
        System.out.println("\n解码后的结果："+result2);
        File file3 = new File("D:\\deHuffman.txt");
        Writer writer1 = new FileWriter(file3);
        writer1.write(result2);
        writer.close();
    }
}
