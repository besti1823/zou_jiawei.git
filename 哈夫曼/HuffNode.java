package hafuman;

public class HuffNode implements Comparable<HuffNode>{
    private char data;
    private double weight;
    private HuffNode left;
    private HuffNode right;
    String codenumber;

    public HuffNode(char data, double weight){
        this.data = data;
        this.weight = weight;
        this.codenumber ="";
    }

    public char  getData() {
        return data;
    }

    public void setData(char data) {
        this.data = data;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public HuffNode getLeft() {
        return left;
    }

    public void setLeft(HuffNode left) {
        this.left = left;
    }

    public HuffNode getRight() {
        return right;
    }

    public void setRight(HuffNode right) {
        this.right = right;
    }

    public String getCodenumber(){
        return codenumber;
    }

    public void setCodenumber(String number){
        codenumber = number;
    }
    @Override
    public String toString(){
        return "data:"+this.data+" weight:"+this.weight+" codenumber:"+this.codenumber+"\n\t";
    }

    @Override
    public int compareTo(HuffNode other) {
        if(other.getWeight() > this.getWeight()){
            return 1;
        }
        if(other.getWeight() < this.getWeight()){
            return -1;
        }
        else
            return 0;
    }

}