package exp8._1;

import pp14.LinkfedStack;

public class LinkedStack {
    LinearNode top;
    public int count=1;
    public  LinearNode bottom;
    public LinkedStack(){
        top=null;
        bottom=null;
    }
    public void push(LinearNode temp){
        if(top==null) {
            top=temp;
            bottom=temp;
        }
        else{
            top.setNext(temp);
            top=temp;
            count++;
        }
    }
    public String pop(){
        String a=top.getElement();
        LinearNode current=bottom;
        if(count==1){top=bottom;}
        else{
        while(current.getNext().getNext()!=null){
            current=current.getNext();
        }
        top=current;}
        return a;
    }
    public String peek(){
        String temp=top.element;
        return temp;
    }
    public String toString(){
        LinearNode current=bottom;
        String result="";
        while(true){
            result+=current.element+" ";
            if(current.next!=null){
                current=current.getNext();
            }
            else break;
        }
        return result;
    }
}
