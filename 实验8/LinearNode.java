package exp8._1;

public class LinearNode {
    public String element;
    public LinearNode next;
    public LinearNode(String temp){
        element=temp;
    }
    public void setNext(LinearNode N){
        next=N;
    }
    public LinearNode getNext(){
        return next;
    }
    public void setElement(String N){
        element=N;
    }
    public String getElement(){
        return element;
    }
}
