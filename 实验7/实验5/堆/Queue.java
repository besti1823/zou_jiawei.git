package com.example.dui;

public interface Queue<T> {
    public void enqueue(T ele) throws E;
    public T dequeue() throws E;
    public T first();
    public boolean isEmpty();
    public int size();
    public String toString();
}
