package com.example.tree;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    EditText a;
    TextView b2;
    TextView b3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        a=findViewById(R.id.edit_text1);
        b2=findViewById(R.id.textView2);
        b3=findViewById(R.id.textView3);
        Button button=findViewById(R.id.button);
        button.setOnClickListener(onClickListener);
    }


    View.OnClickListener onClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String str1=a.getText().toString();
                int a,b,c,d,count;
                int[] h=new int[10];
                Arrays.fill(h,0);
                leaf root=null;
                String f,g;
//                Scanner scan=new Scanner(System.in);
//                System.out.println("请输入数组数字(空格隔开)");
//                f=scan.nextLine();
                String[] e=str1.split(" ");
                for(c=0;c<e.length;c++){
                    h[c]= Integer.parseInt(e[c]);
                }
                //构造树
                for(a=0;a<e.length;a++){
                    leaf temp=new leaf(h[a]);
                    if(root==null){
                        root=temp;
                    }
                    else{
                        leaf current=root;
                        while(true) {
                            if (temp.root > current.root) {
                                if(current.right==null) {current.right=temp; break;}
                                else current=current.right;
                            }
                            if(temp.root<current.root){
                                if(current.left==null){current.left=temp;break;}
                                else current=current.left;
                            }
                        }
                    }
                }

                String srt2=root.dayin(root);
                b3.setText(srt2);
            }

    };
}
