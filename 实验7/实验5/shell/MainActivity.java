package com.example.shell;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    EditText a;
    TextView b2;
    TextView b3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        a=findViewById(R.id.edit_text1);
        b2=findViewById(R.id.textView2);
        b3=findViewById(R.id.textView3);
        Button button=findViewById(R.id.button);
        button.setOnClickListener(onClickListener);

    }
    View.OnClickListener onClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String str1 = a.getText().toString();
            int gap, a, d, count = 0, temp, l, okk;
            int[] c = new int[10];
            Arrays.fill(c, 0);
            String e, ok = "";
//            Scanner scan = new Scanner(System.in);
//            System.out.println("输入数组数字：(空格隔开)");
//            e = scan.nextLine();
            String[] f = str1.split(" ");
            count = f.length;
            for (a = 0; a < f.length; a++) {
                c[a] = Integer.parseInt(f[a]);
            }
            for (gap = f.length / 2; gap > 0; gap = gap / 2) {
                for (d = gap; d <= f.length - 1; d++) {
                    l = d;
                    temp = c[l];
                    if (c[l] < c[l - gap]) {
                        while (l - gap >= 0 && temp < c[l - gap]) {
                            c[l] = c[l - gap];
                            l -= gap;
                        }
                        c[l] = temp;
                    }
                }
            }
            for (okk = 0; okk < f.length; okk++) {
                ok += c[okk] + " ";
            }
            b3.setText(ok);
        }

    };
}
