package exp7_1;//package exp7_1;
//
//
//
import exp7_1.Searching;
import junit.framework.TestCase;
import org.junit.*;

public class testlinearSearchTest extends TestCase {
    Searching a = new Searching();
    int[] okk={0,2,3,1,5};//从第二个数开始输入

    @Test
    public void test(){
        assertEquals(true,a.linearSearch(okk,1));
    }//正常
    @Test
    public void test2(){
        assertEquals(false,a.linearSearch(okk,9));
    }//异常
    @Test
    public void test3(){
        assertEquals(true,a.linearSearch(okk,1));
    }//边界
    public void test4(){
        assertEquals(true,a.linearSearch(okk,5));
    }//边界
    public void test5(){
        assertEquals(false,a.linearSearch(okk,0));
    }//异常
}