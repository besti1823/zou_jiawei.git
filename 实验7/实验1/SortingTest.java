package exp7_1;

import junit.framework.TestCase;
import org.junit.Test;

public class SortingTest extends TestCase {
    Sorting a=new Sorting();
    int b[]={5,3,9,7,2};
    int c[]={7,3,5,2,0,9,4};
    @Test
    public  void test(){
        assertEquals("2 3 5 7 9 ",a.sort(b));//正序

    }
    @Test
    public  void test2(){
        assertEquals("9 7 5 3 2 ",a.sort2(b));//逆序
    }
    public  void test3(){
        assertEquals("0 2 3 4 5 7 9 ",a.sort(c));//正序
    }
    public  void test4(){
        assertEquals("9 7 5 4 3 2 0 ",a.sort2(c));//逆序
    }
    public  void test5(){
        assertEquals("9 7 5 3 4 0 2 ",a.sort2(c));//异常！！！！！！！！！
    }
}