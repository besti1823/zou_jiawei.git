package exp7_3;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Searching {
    //顺序查找
    public boolean SequenceSearch(int[] a, int find) {
        for (int b = 0; b < a.length; b++) {
            if (a[b] == find) return true;
        }
        return false;
    }

    public boolean BinarySearch(int[] a, int find) {
        int b, mid, last, first;
        for (int c = 0; c < a.length; c++) {
            for (int d = c; d < a.length; d++) {
                if (a[d] < a[c]) {
                    b = a[d];
                    a[d] = a[c];
                    a[c] = b;
                }
            }
        }
        first = 0;
        last = a.length - 1;
        mid = (first + last) / 2;
        while (first <= last) {
            if (find == a[mid]) return true;
            if (find > a[mid]) {
                first = mid + 1;
                mid = (first + last) / 2;}
                if (find < a[mid]) {
                    last = mid - 1;
                    mid = (last + first) / 2;
                }
            }

        return false;
    }

    public boolean InsertionSearch(int[] a, int find) {
        int b, mid, last, first;
        for (int c = 0; c < a.length; c++) {
            for (int d = c; d < a.length; d++) {
                if (a[d] < a[c]) {
                    b = a[d];
                    a[d] = a[c];
                    a[c] = d;
                }
            }
        }
        first = 0;
        last = a.length - 1;
        mid = first + (find - a[first]) / (a[last] - a[first]) * (last - first);
        while (first < last) {
            if (find == a[mid]) return true;
            if (find > a[mid]) {
                first = mid + 1;
                mid = first + (find - a[first]) / (a[last] - a[first]) * (last - first);
                if (find < a[mid]) {
                    last = mid - 1;
                    mid = first + (find - a[first]) / (a[last] - a[first]) * (last - first);
                }
            }
        }
        return false;
    }

    public boolean erchashu(int a[], int find) {
        int b, c, d, e;
        int count = 0;
        leaf root = null;
        String n = "123";

        while (count != a.length) {

            leaf kk = new leaf(a[count]);
            if (root == null)
                root = kk;
            else {
                leaf current = root;
                while (true) {
                    if (kk.a > current.a) {
                        if (current.right == null) {
                            current.right = kk;
                            break;
                        } else {
                            current = current.right;
                        }
                    }
                    if (kk.a <= current.a) {
                        if (current.left == null) {
                            current.left = kk;
                            break;
                        } else current = current.left;
                    }
                }
            }
            count++;
        }
        leaf okk = root;
        while (okk != null) {
            if (find == okk.a) {
                return true;
            }
            if (find > okk.a) {
                okk = okk.right;
            }
            if (find < okk.a) okk = okk.left;
        }
        return false;
    }
    public boolean haxi(int[] a,int find){
        int count=0;
        int t=1;
        int c;
        int[] k=new int[100];
        Arrays.fill(k,200);
        while(count!=a.length){
            c=a[count]%11;
            if(k[c]==200)
                k[c]=a[count];
            else if(k[c]!=200){
                for(t=1;t<=100-c;t++){
                    if(k[c+t]==200) break;
                }
                k[c+t]=a[count];
            }

           count++;
        }
        int b,l= 1;
        b=find%11;
        if(k[b]==find){
            return true;
        }
        else if(k[b]!=find){
            for(l=1;l<=count;l++){
                if(k[b+l]==find) return true;
            }
        }
        return false;
    }
    public boolean fenkuai(int[] a,int find){
        int b,k;
        int e=a.length/2;
        int f=e/2;
        for (int c = 0; c < a.length; c++) {
            for (int d = c; d < a.length; d++) {
                if (a[d] < a[c]) {
                    b = a[d];
                    a[d] = a[c];
                    a[c] = b;
                }
            }
        }
        if(find>=a[0]&&find<=a[f]){
            for(k=0;k<=f;k++){
                if(a[k]==find) return true;
            }
        }
        if(find>a[f]&&find<a[e]){
            for(k=f+1;k<e;k++){
                if(a[k]==find) return true;
            }
        }
        if(find>=a[e]&&find<=a[a.length-1]){
            for(k=e;k<=a.length-1;k++){
                if(a[k]==find) return true;
            }

        }
        return false;
    }
    int max_size=20;//斐波那契数组的长度

    /*构造一个斐波那契数组*/
    void Fibonacci(int[] F)
    {
        F[0]=0;
        F[1]=1;
        for(int i=2;i<max_size;++i)
            F[i]=F[i-1]+F[i-2];
    }

    /*定义斐波那契查找法*/
    public boolean FibonacciSearch(int[] a, int n, int key)  //a为要查找的数组,n为要查找的数组长度,key为要查找的关键字
    {
        int low=0;
        int high=n-1;

        int[] F=new int[max_size];
        Fibonacci(F);//构造一个斐波那契数组F

        int k=0;
        while(n>F[k]-1)//计算n位于斐波那契数列的位置
            ++k;

        int[] temp;//将数组a扩展到F[k]-1的长度
        temp=new int [F[k]-1];

        for(int i=0;i<F[k]-1;++i){
            temp[i]=a[i];
            if(i>=n) temp[i]=a[n];
        }


        while(low<=high)
        {
            int mid=low+F[k-1]-1;
            if(key<temp[mid])
            {
                high=mid-1;
                k-=1;
            }
            else if(key>temp[mid])
            {
                low=mid+1;
                k-=2;
            }
            else
            {
                if(mid<n)
                    return true; //若相等则说明mid即为查找到的位置
                else
                    return false; //若mid>=n则说明是扩展的数值,返回n-1
            }
        }
        return false;
    }


}
