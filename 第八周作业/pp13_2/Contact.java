package work8.pp13_2;

public class Contact implements Comparable{
    public String firstname,lastname,phone;
    public Contact(String first,String last,String telephone){
        firstname=first;
        lastname=last;
        phone=telephone;
    }
    public String toString(){
        return lastname+", "+firstname+": "+phone;
    }
    @Override
    public int compareTo(Object other) {
        int result;
//        if(phone.equals(((Contact)other).phone))
            result=phone.compareTo((((Contact)other).phone));
        return result;
    }
}
