package work8.pp13_2;

import work8.pp13_2.Contact;
import work8.pp13_2.Searching;

public class SearchPlayerList {
    public  static void main(String[] args){
        Contact[] players=new Contact[7];
        Contact play;
        players[0] =new Contact("Rodge","Federer","610-555-7384");
        players[1] =new Contact("Andy","Roddick","215-555-3827");
        players[2] =new Contact("Rodge","Federer","733-555-2969");
        players[3] =new Contact("Rodge","Federer","663-555-3984");
        players[4] =new Contact("Rodge","Federer","464-555-3489");
        players[5] =new Contact("Eleni","Daniilidou","322-555-2284");
        players[6] =new Contact("Rodge","Federer","243-555-2837");
        Contact target=new Contact("Rodge","Federer","322-555-2284");
        Contact found=(Contact) Searching.linearSearching(players,target);
        System.out.println("下面是线性查找");
        if(found==null)
            System.out.println("play was not found");
        else
            System.out.println("found: "+found);//xianxing

        System.out.println("下面是二分法");
        int a,b,c,d;
        for(a=0;a<players.length;a++){
            for(b=a;b<players.length;b++){
                if((players[a].phone.compareTo(players[b].phone)>0)){
                    play=players[a];
                    players[a]=players[b];
                    players[b]=play;
                }
            }
        }
        Contact foun=(Contact)Searching.binarySearch(players,target);
        if(foun==null)
            System.out.println("play was not found");
        else
            System.out.println("found: "+foun);
    }
}
