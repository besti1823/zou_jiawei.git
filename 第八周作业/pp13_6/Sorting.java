public class Sorting
{
    protected static int countp=0;
    protected static int countm=0;
    protected static long s1;
    protected static long s2;
    protected static long s3;
    protected static long s4;
    protected static long s5;
    public static void main(String[] args){
        Comparable[] a={1,6,34,22,14,65,23,12,2,5,38,45};
        Comparable[] b={1,6,34,22,14,65,23,12,2,5,38,45};
        Comparable[] c={1,6,34,22,14,65,23,12,2,5,38,45};
        Comparable[] d={1,6,34,22,14,65,23,12,2,5,38,45};
        Comparable[] e={1,6,34,22,14,65,23,12,2,5,38,45};
        int t=selectionSort(a);
        int t1=insertionSort(b);
        int t2=bubbleSort(c);
        quickSort(d,0,11);
        mergeSort(e,0,11);
        System.out.println("count: "+t+"   time: "+s1+"ns");
        System.out.println("count: "+t1+"   time: "+s2+"ns");
        System.out.println("count: "+t2+"   time: "+s3+"ns");
        System.out.println("count: "+countp+"   time: "+s4+"ns");
        System.out.println("count: "+countm+"   time: "+s5+"ns");
    }





    public static int selectionSort (Comparable[] data)
    {
        int min;
        int count=0;
        long Time1=System.nanoTime();
        for (int index = 0; index < data.length-1; index++)
        {
            min = index;
            for (int scan = index+1; scan < data.length; scan++)
                if (data[scan].compareTo(data[min]) < 0) {
                    min = scan;
                    count++;
                }
            swap (data, min, index);
        }
        long Time2=System.nanoTime();
        s1=Time2-Time1;
        return count;
    }


    private static void swap (Comparable[] data, int index1, int index2)
    {
        Comparable temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    public static int insertionSort (Comparable[] data)
    {
        int count=0;
        long Time1=System.nanoTime();
        for (int index = 1; index < data.length; index++)
        {
            Comparable key = data[index];
            int position = index;

            while (position > 0 && data[position-1].compareTo(key) > 0)
            {
                data[position] = data[position-1];
                position--;
                count++;
            }

            data[position] = key;
        }
        long Time2=System.nanoTime();
        s2=Time2-Time1;
        return count;
    }

    public static int bubbleSort (Comparable[] data)
    {
        int position, scan;
        int count=0;
        long Time1=System.nanoTime();
        for (position = data.length - 1; position >= 0; position--)
        {
            for (scan = 0; scan <= position - 1; scan++)
                if (data[scan].compareTo(data[scan+1]) > 0){
                    swap (data, scan, scan+1);
                    count++;
                }
        }
        long Time2=System.nanoTime();
        s3=Time2-Time1;
        return count;
    }


    public static int quickSort (Comparable[] data, int min, int max)
    {
        int pivot;
        long Time1=System.nanoTime();
        if (min < max)
        {
            countp++;
            pivot = partition (data, min, max);
            quickSort(data, min, pivot-1);
            quickSort(data, pivot+1, max);
        }
        long Time2=System.nanoTime();
        s4=Time2-Time1;
        return countp;
    }

    private static int partition (Comparable[] data, int min, int max)
    {
        Comparable partitionValue = data[min];

        int left = min;
        int right = max;

        while (left < right)
        {
            while (data[left].compareTo(partitionValue) <= 0 && left < right) {
                left++;
                countp++;
            }
            while (data[right].compareTo(partitionValue) > 0) {
                right--;
                countp++;
            }

            if (left < right) {
                swap(data, left, right);
                countp++;
            }
        }

        swap (data, min, right);

        return right;
    }

    public static int mergeSort (Comparable[] data, int min, int max)
    {
        long Time1=System.nanoTime();//运算时间
        if (min < max)
        {
            int mid = (min + max) / 2;
            mergeSort (data, min, mid);
            mergeSort (data, mid+1, max);
            merge (data, min, mid, max);
        }
        long Time2=System.nanoTime();
        s5=Time2-Time1;
        return countm;
    }

    public static void merge (Comparable[] data, int first, int mid,
                              int last)
    {
        Comparable[] temp = new Comparable[data.length];

        int first1 = first, last1 = mid;
        int first2 = mid+1, last2 = last;
        int index = first1;


        while (first1 <= last1 && first2 <= last2)
        {
            countm++;
            if (data[first1].compareTo(data[first2]) < 0)
            {
                temp[index] = data[first1];
                first1++;
            }
            else
            {
                temp[index] = data[first2];
                first2++;
            }
            index++;
        }


        while (first1 <= last1)//比较
        {
            countm++;
            temp[index] = data[first1];
            first1++;
            index++;
        }
        while (first2 <= last2)
        {
            countm++;
            temp[index] = data[first2];
            first2++;
            index++;
        }

        for (index = first; index <= last; index++)
            data[index] = temp[index];
    }
}