package work10;

import exp8._1.LinkedStack;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
//图表示图形的邻接矩阵实现。
public class Graph<T> implements GraphADT<T>
{
    protected final int DEFAULT_CAPACITY = 5;
    protected int numVertices;    // 图中的顶点数
    protected boolean[][] adjMatrix;    // 邻接矩阵
    protected T[] vertices;    // 顶点的值
    protected int modCount;
    //创建一个空图形。
    public Graph()
    {
        numVertices = 0;
        this.adjMatrix = new boolean[DEFAULT_CAPACITY][DEFAULT_CAPACITY];
        this.vertices = (T[])(new Object[DEFAULT_CAPACITY]);
    }
    //返回邻接矩阵的字符串表示形式。
    public String toString()
    {
        if (numVertices == 0)
            return "Graph is empty";
        String result = new String("");
        result += "Adjacency Matrix\n";
        result += "----------------\n";
        result += "index\t";
        for (int i = 0; i < numVertices; i++)
        {
            result += "" + i;
            if (i < 10)
                result += " ";
        }
        result += "\n\n";
        for (int i = 0; i < numVertices; i++)
        {
            result += "" + i + "\t";
            for (int j = 0; j < numVertices; j++)
            {
                if (adjMatrix[i][j])
                    result += "1 ";
                else
                    result += "0 ";
            }
            result += "\n";
        }
        result += "\n\nVertex Values";
        result += "\n-------------\n";
        result += "index\tvalue\n\n";
        for (int i = 0; i < numVertices; i++)
        {
            result += "" + i + "\t";
            result += vertices[i].toString() + "\n";
        }
        result += "\n";
        return result;
    }
    //添加一条边
    public void addEdge(int index1, int index2)

    {
        if (indexIsValid(index1) && indexIsValid(index2))
        {
            adjMatrix[index1][index2] = true;
            adjMatrix[index2][index1] = true;
            modCount++;
        }
    }
    public void addEdge(T vertex1, T vertex2)
    {
        addEdge(getIndex(vertex1), getIndex(vertex2));
    }

    //删除一条边
    public void removeEdge(int index1, int index2)

    {
        if (indexIsValid(index1) && indexIsValid(index2))
        {
            adjMatrix[index1][index2] = false;
            adjMatrix[index2][index1] = false;
            modCount++;
        }
    }
    public void removeEdge(T vertex1, T vertex2)
    {
        removeEdge(getIndex(vertex1),getIndex(vertex2));

    }


    //将顶点添加到图形中, 扩展图形的容量如有需要。 它还将对象与顶点相关联。
    public void addVertex(T vertex)
    {
        if ((numVertices + 1) == adjMatrix.length)
            expandCapacity();
        vertices[numVertices] = vertex;
        for (int i = 0; i < numVertices; i++)
        {
            adjMatrix[numVertices][i] = false;
            adjMatrix[i][numVertices] = false;
        }
        numVertices++;
        modCount++;
    }

    //从图形中移除给定索引处的顶点。 请注意, 这可能会影响其他顶点的索引值。

    public void removeVertex(int index)
    {
        if (index >= numVertices) {
            System.out.println("没有此顶点！");
        }
        for (int i = 0; i < numVertices; i++) {
            adjMatrix[index][i] = false;
            adjMatrix[i][index] = false;
        }
        if (index == numVertices - 1)
            numVertices--;
        for (int i = index + 1; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                adjMatrix[i - 1][j] = adjMatrix[i][j];
            }
        }
        numVertices--;
    }
    public void removeVertex(T vertex)
    {
        removeVertex(getIndex(vertex));
    }

    //返回从给定索引开始执行深度优先遍历的迭代器。
    public Iterator<T> iteratorDFS(int startIndex)
    {
        Integer x;
        boolean found;
        StackADT<Integer> traversalStack = (StackADT<Integer>) new LinkedStack<Integer>();
        UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();
        boolean[] visited = new boolean[numVertices];
        if (!indexIsValid(startIndex))
            return resultList.iterator();
        for (int i = 0; i < numVertices; i++)
            visited[i] = false;
        traversalStack.push(new Integer(startIndex));
        resultList.addToRear(vertices[startIndex]);
        visited[startIndex] = true;
        while (!traversalStack.isEmpty())
        {
            x = traversalStack.peek();
            found = false;
            //查找与 x 相邻但尚未访问的顶点然后把它推到堆栈上
            for (int i = 0; (i < numVertices) && !found; i++)
            {
                if (adjMatrix[x.intValue()][i] && !visited[i])
                {
                    traversalStack.push(new Integer(i));
                    resultList.addToRear(vertices[i]);
                    visited[i] = true;
                    found = true;
                }
            }
            if (!found && !traversalStack.isEmpty())
                traversalStack.pop();
        }
        return new GraphIterator(resultList.iterator());
    }
    public Iterator<T> iteratorDFS(T startVertex)
    {
        return iteratorDFS(getIndex(startVertex));
    }

//返回从给定索引开始执行广度优先遍历的迭代器。

    public Iterator<T> iteratorBFS(int startIndex)

    {
        Integer x;
        QueueADT<Integer> traversalQueue = (QueueADT<Integer>) new LinkedQueue<Integer>();
        UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();
        if (!indexIsValid(startIndex))
            return resultList.iterator();
        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++)
            visited[i] = false;
        traversalQueue.enqueue(new Integer(startIndex));
        visited[startIndex] = true;
        while (!traversalQueue.isEmpty())
        {
            x = traversalQueue.dequeue();
            resultList.addToRear(vertices[x.intValue()]);
            for (int i = 0; i < numVertices; i++)
            {
                if (adjMatrix[x.intValue()][i] && !visited[i])
                {
                    traversalQueue.enqueue(new Integer(i));
                    visited[i] = true;
                }
            }
        }
        return new GraphIterator(resultList.iterator());
    }
    public Iterator<T> iteratorBFS(T startVertex)
    {
        return iteratorBFS(getIndex(startVertex));
    }
    //返回一个迭代器, 其中包含位于两个给定顶点之间最短路径中的顶点的索引。
    protected Iterator<Integer> iteratorShortestPathIndices
    (int startIndex, int targetIndex)
    {
        int index = startIndex;
        int[] pathLength = new int[numVertices];
        int[] predecessor = new int[numVertices];
        QueueADT<Integer> traversalQueue = (QueueADT<Integer>) new LinkedQueue<Integer>();
        UnorderedListADT<Integer> resultList =
                new ArrayUnorderedList<Integer>();
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) ||
                (startIndex == targetIndex))
            return resultList.iterator();
        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++)
            visited[i] = false;
        traversalQueue.enqueue(new Integer(startIndex));
        visited[startIndex] = true;
        pathLength[startIndex] = 0;
        predecessor[startIndex] = -1;
        while (!traversalQueue.isEmpty() && (index != targetIndex))
        {
            index = (traversalQueue.dequeue()).intValue();
            //更新当前索引处的顶点旁边的每个未访问顶点的路径长度。
            for (int i = 0; i < numVertices; i++)
            {
                if (adjMatrix[index][i] && !visited[i])
                {
                    pathLength[i] = pathLength[index] + 1;
                    predecessor[i] = index;
                    traversalQueue.enqueue(new Integer(i));
                    visited[i] = true;
                }
            }
        }
        if (index != targetIndex)
            return resultList.iterator();
        StackADT<Integer> stack = new LinkedStack<Integer>();
        index = targetIndex;
        stack.push(new Integer(index));
        do
        {
            index = predecessor[index];
            stack.push(new Integer(index));
        } while (index != startIndex);
        while (!stack.isEmpty())
            resultList.addToRear(((Integer)stack.pop()));
        return new GraphIndexIterator(resultList.iterator());
    }
    // 返回一个迭代器, 其中包含之间的最短路径
    public Iterator<T> iteratorShortestPath(int startIndex,
                                            int targetIndex)
    {
        UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex))
            return resultList.iterator();
        Iterator<Integer> it = iteratorShortestPathIndices(startIndex,
                targetIndex);
        while (it.hasNext())
            resultList.addToRear(vertices[((Integer)it.next()).intValue()]);
        return new GraphIterator(resultList.iterator());
    }
    public Iterator<T> iteratorShortestPath(T startVertex, T targetVertex)
    {
        return iteratorShortestPath(getIndex(startVertex),
                getIndex(targetVertex));
    }

    //返回网络中最小权重路径的权重。

    public int shortestPathLength(int startIndex, int targetIndex)

    {
        int result = 0;
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex))
            return 0;
        int index1, index2;
        Iterator<Integer> it = iteratorShortestPathIndices(startIndex,
                targetIndex);
        if (it.hasNext())
            index1 = ((Integer)it.next()).intValue();
        else
            return 0;
        while (it.hasNext())
        {
            result++;
            it.next();
        }
        return result;
    }
    public int shortestPathLength(T startVertex, T targetVertex)
    {
        return shortestPathLength(getIndex(startVertex), getIndex(targetVertex));
    }

    //返回图形的最小生成树。
    public Graph getMST()
    {
        int x, y;
        int[] edge = new int[2];
        StackADT<int[]> vertexStack = new LinkedStack<int[]>();
        Graph<T> resultGraph = new Graph<T>();
        if (isEmpty() || !isConnected())
            return resultGraph;
        resultGraph.adjMatrix = new boolean[numVertices][numVertices];
        for (int i = 0; i < numVertices; i++)
            for (int j = 0; j < numVertices; j++)
                resultGraph.adjMatrix[i][j] = false;
        resultGraph.vertices = (T[])(new Object[numVertices]);
        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++)
            visited[i] = false;
        edge[0] = 0;
        resultGraph.vertices[0] = this.vertices[0];
        resultGraph.numVertices++;
        visited[0] = true;
        // 将与顶点0相邻的所有边添加到堆栈中。
        for (int i = 0; i < numVertices; i++)
        {
            if (!visited[i] && this.adjMatrix[0][i])
            {
                edge[1] = i;
                vertexStack.push(edge.clone());
                visited[i] = true;
            }
        }
        while ((resultGraph.size() < this.size()) && !vertexStack.isEmpty())
        {
            // 弹出堆栈上的边, 并将其添加到结果图。
            edge = vertexStack.pop();
            x = edge[0];
            y = edge[1];
            resultGraph.vertices[y] = this.vertices[y];
            resultGraph.numVertices++;
            resultGraph.adjMatrix[x][y] = true;
            resultGraph.adjMatrix[y][x] = true;
            visited[y] = true;
            for (int i = 0; i < numVertices; i++)
            {
                if (!visited[i] && this.adjMatrix[i][y])
                {
                    edge[0] = y;
                    edge[1] = i;
                    vertexStack.push(edge.clone());
                    visited[i] = true;
                }
            }
        }
        return resultGraph;
    }
    //创建新数组来存储图形的内容。
    protected void expandCapacity()
    {
        T[] largerVertices = (T[])(new Object[vertices.length*2]);
        boolean[][] largerAdjMatrix =
                new boolean[vertices.length*2][vertices.length*2];
        for (int i = 0; i < numVertices; i++)
        {
            for (int j = 0; j < numVertices; j++)
            {
                largerAdjMatrix[i][j] = adjMatrix[i][j];
            }
            largerVertices[i] = vertices[i];
        }
        vertices = largerVertices;
        adjMatrix = largerAdjMatrix;
    }
//返回图形中的顶点数。

    public int size()
    {
        return numVertices;
    }
    //如果图形为空, 否则返回 true。
    public boolean isEmpty()
    {
        if (numVertices == 0){
            return true;
        }
        else {
            return false;
        }
    }
    //如果图形连接, 则返回 true, 否则返回 false。
    public boolean isConnected()
    {
        boolean result = true;
        for (int i=0;i<adjMatrix.length;i++){
            for(int j=0;j<adjMatrix.length;j++){
                if(adjMatrix[i][j]==false){
                    result = false;
                }
            }
        }
        return result;
    }
    //返回顶点第一次出现的索引值。
    public int getIndex(T vertex)
    {
        int index = numVertices-1;
        for (int i=0;i<numVertices;i++){
            if (vertex.equals(vertices[i])){
                index = 1;
                break;
            }
        }
        return index;
    }
    //如果给定的索引有效, 则返回 true。
    protected boolean indexIsValid(int index)
    {
        if (index <vertices.length){
            return true;
        }
        return false;
    }
    //返回顶点数组的副本。
    public Object[] getVertices()
    {
        return vertices;
    }
    // 内部类, 用于表示此图元素上的迭代器
    protected class GraphIterator implements Iterator<T>
    {
        private int expectedModCount;
        private Iterator<T> iter;
        //使用指定的迭代器设置此迭代器。
        public GraphIterator(Iterator<T> iter)
        {
            this.iter = iter;
            expectedModCount = modCount;
        }
        //如果此迭代器至少有一个其他元素, 则返回 true
        public boolean hasNext() throws ConcurrentModificationException
        {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();
            return (iter.hasNext());
        }
        //返回迭代中的下一个元素。如果没有
        public T next() throws NoSuchElementException
        {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }
        // 不支持删除操作。
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }
    //内部类, 用于在此图的索引上表示迭代器
    protected class GraphIndexIterator implements Iterator<Integer>
    {
        private int expectedModCount;
        private Iterator<Integer> iter;
        // 使用指定的迭代器设置此迭代器。
        public GraphIndexIterator(Iterator<Integer> iter)
        {
            this.iter = iter;
            expectedModCount = modCount;
        }
        //如果此迭代器至少有一个其他元素, 则返回 true
        public boolean hasNext() throws ConcurrentModificationException
        {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();
            return (iter.hasNext());
        }
        // 返回迭代中的下一个元素。如果没有
        public Integer next() throws NoSuchElementException
        {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }
        // 不支持删除操作。
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }
}