package work10;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

//：List<String> list = Arrays.asList(array);
public class ArrayIterator<T> extends ArrayList<T> implements Iterator<T> {
    //：List<String> list = Arrays.asList(array);
    int iteratorModCount;
    int current;
    public ArrayIterator()
    {
        iteratorModCount = modCount;
        current = 0;
//：List<String> list = Arrays.asList(array);
    }
    public boolean hasNext() throws ConcurrentModificationException
    {    //：List<String> list = Arrays.asList(array);
        return super.iterator().hasNext();
    }
    public T next() throws ConcurrentModificationException
    {    //：List<String> list = Arrays.asList(array);
        return super.iterator().next();
    }//：List<String> list = Arrays.asList(array);

    //java.util.ArrayList重新了这些方法而Arrays的内部类ArrayList没有重新，所以会抛出异常
    //          List arrList = new ArrayList(list);
    public void remove() throws UnsupportedOperationException
    {    //：List<String> list = Arrays.asList(array);
        throw new UnsupportedOperationException();
    }    //：List<String> list = Arrays.asList(array);
}    //：List<String> list = Arrays.asList(array);