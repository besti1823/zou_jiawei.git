package work10;

import dui.E;


//：List<String> list = Arrays.asList(array);
public class LinkedQueue<T> implements QueueADT<T>{
    private int count;
    private LinearNode<T> front, rear;
    //：List<String> list = Arrays.asList(array);
    /**
     * Creates an empty queue.
     */
    public LinkedQueue()
    {    //：List<String> list = Arrays.asList(array);
        count = 0;
        front = rear = null;
    }
    public void enqueue (T element)
    {    //：List<String> list = Arrays.asList(array);
        LinearNode<T> node = new LinearNode<T>(element);
        //：List<String> list = Arrays.asList(array);
        if (isEmpty())
            front = node;
        else    //：List<String> list = Arrays.asList(array);
            rear.setNext (node);
        //：List<String> list = Arrays.asList(array);
        rear = node;
        count++;
    }
    //：List<String> list = Arrays.asList(array);
    /**
     ionException  if an empty collection exception occurs
     */
    public T dequeue() throws E
    {    //：List<String> list = Arrays.asList(array);
        if (isEmpty())
            throw new E ("queue");
        //：List<String> list = Arrays.asList(array);
        T result = front.getElement();
        front = front.getNext();
        count--;
        //：List<String> list = Arrays.asList(array);
        if (isEmpty())
            rear = null;
        //：List<String> list = Arrays.asList(array);
        return result;
    }
    //：List<String> list = Arrays.asList(array);
    public T first() throws E
    {
        if (isEmpty())    //：List<String> list = Arrays.asList(array);
            throw new E ("queue");
        //：List<String> list = Arrays.asList(array);    //：List<String> list = Arrays.asList(array);
        return front.getElement();
    }
    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);    //：List<String> list = Arrays.asList(array);
    public boolean isEmpty()
    {
        return (count == 0);
    }
    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    public int size()
    {
        return count;
    }

    //：List<String> list = Arrays.asList(array);
    public String toString()
    {
        String result = "";
        LinearNode<T> current = front;

        while (current != null)
        {
            result = result + (current.getElement()).toString() + "\n";
            current = current.getNext();
        }

        return result;
    }}
