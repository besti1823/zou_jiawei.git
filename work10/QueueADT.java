package work10;


import dui.E;

public interface QueueADT<T>
{

    //：List<String> list = Arrays.asList(array);
    public void enqueue(T element);

    public T dequeue() throws E;

    /**
     lement in this queue
     */
    public T first() throws E;

    //：List<String> list = Arrays.asList(array);
    public boolean isEmpty();//：List<String> list = Arrays.asList(array);
//：List<String> list = Arrays.asList(array);

    public int size();
//：List<String> list = Arrays.asList(array);

    public String toString();
}